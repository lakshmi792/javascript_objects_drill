const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function invert(object=testObject){
    if(object.length==0)
    {
        return object={};
    } 
    if(typeof(object)!='object')
    {
        return object={};
    }
    let answer={};
    for(let key in object)
    {
        let ans=object[key];
        answer[ans]=key;
    }
    return answer;

}
let result=invert(testObject);
//console.log(result);

module.exports=invert;