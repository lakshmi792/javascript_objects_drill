const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let add = { name: 'Bruce Wayne' };

function defaults(add = add, obj = testObject) {

    let answer = {};

    if (add.length == 0 && obj.length == 0) {
        return {};
    }
    if (add.length == 0) {
        return obj;
    }
    if (obj.length == 0) {
        return add;
    }

    if (typeof (add) != 'object' && typeof (obj) != 'object') {
        return {};
    }
    if (typeof (obj) != 'object') {
        return add;
    }
    if (typeof (add) != 'object') {
        return obj;
    }


    for (let key in obj) {
        if (add[key]) {
            answer[key] = add[key];
        }
        else {
            answer[key] = obj[key];
        }
    }


    return answer;
}
let result=defaults(add,testObject);
//console.log(result);

module.exports = defaults;
