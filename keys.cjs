const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function problem1(object = testObject) {
    if (object.length == 0) {
        return object = {};
    }
    if (typeof (object) !== 'object') {
        return object = {};
    }
    let answer = [];
    for (let key in object) {
        answer.push(key);
    }
    return answer;
}
//let ans=problem1(testObject);
//console.log(ans);

module.exports = problem1;