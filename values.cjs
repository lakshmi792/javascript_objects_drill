const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function values(object = testObject) {
    if (object.length == 0) {
        return object = {};
    }
    if (typeof (object) != 'object') {
        return object = {};
    }
    let answer = [];
    for (let value in object) {
        answer.push(object[value]);
    }
    return answer;
}
//let result=values(testObject);
//console.log(result);

module.exports = values;