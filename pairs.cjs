const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function pairs(object = testObject) {
    if (object.length == 0) {
        return {};
    }
    if (typeof (object) != 'object') {
        return {};
    }
    let answer = [];
    for (let key in object) {
        let res = [];
        res.push(String(key), object[key]);
        answer.push(res);
    }
    return answer;
}
let result = pairs(testObject);
//console.log(result);

module.exports = pairs;