const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function callback(val, key) {

    return val + 5;
}

function mapObject(object=testObject, cb=callback) {
    if (object.length == 0) {
        return object = {};
    }
    if (typeof (object) != 'object') {
        return object = {};
    }
    if(typeof(cb)!='function')
    {
        return cb={};
    }
    let answer = {};
    for (let key in object) {
        if (cb(object[key])) {
            answer[key] = cb(object[key], key);
        }

    }
    return answer;

}

let result = mapObject(testObject, callback);
//console.log(result);

module.exports = mapObject;